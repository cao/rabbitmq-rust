use lapin::{
    options::{BasicPublishOptions, QueueDeclareOptions},
    types::FieldTable,
    BasicProperties, Connection, ConnectionProperties,
};

#[tokio::main]
async fn main() {
    let url = "amqps://eaxyapkn:tRYdniZqsBBge5J3DXPFdFCqLQGew7FA@gerbil.rmq.cloudamqp.com/eaxyapkn";
    let options = ConnectionProperties::default()
        .with_executor(tokio_executor_trait::Tokio::current())
        .with_reactor(tokio_reactor_trait::Tokio);

    let connection = Connection::connect(url, options).await.unwrap();
    let channel = connection.create_channel().await.unwrap();

    let queue = channel
        .queue_declare(
            "hello-world",
            QueueDeclareOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();
    let payload = "你好，世界".as_bytes();
    channel
        .basic_publish(
            "",
            queue.name().as_str(),
            BasicPublishOptions::default(),
            payload,
            BasicProperties::default(),
        )
        .await
        .unwrap()
        .await
        .unwrap();
    println!("[x] sent: {:?}", String::from_utf8_lossy(payload));
}

use lapin::{
    options::{BasicPublishOptions, ExchangeDeclareOptions, QueueBindOptions, QueueDeclareOptions},
    types::FieldTable,
    BasicProperties, Connection, ConnectionProperties,
};

#[tokio::main]
async fn main() {
    let url = "amqps://eaxyapkn:tRYdniZqsBBge5J3DXPFdFCqLQGew7FA@gerbil.rmq.cloudamqp.com/eaxyapkn";
    let options = ConnectionProperties::default()
        .with_executor(tokio_executor_trait::Tokio::current())
        .with_reactor(tokio_reactor_trait::Tokio);

    let conn = Connection::connect(url, options).await.unwrap();
    let channel = conn.create_channel().await.unwrap();
    channel
        .exchange_declare(
            "foo-exchange",
            lapin::ExchangeKind::Topic,
            ExchangeDeclareOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();
    let queue = channel
        .queue_declare(
            "foo-queue",
            QueueDeclareOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();
    channel
        .queue_bind(
            queue.name().as_str(),
            "foo-exchange",
            "foo",
            QueueBindOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();
    let payload = "Hello, 世界👋".as_bytes();
    channel
        .basic_publish(
            "foo-exchange",
            "foo",
            BasicPublishOptions::default(),
            payload,
            BasicProperties::default(),
        )
        .await
        .unwrap()
        .await
        .unwrap();
    println!("[x] sent {}", String::from_utf8_lossy(payload));
}

use lapin::{
    message::DeliveryResult,
    options::{
        BasicAckOptions, BasicConsumeOptions, ExchangeDeclareOptions, QueueBindOptions,
        QueueDeclareOptions,
    },
    types::FieldTable,
    Connection, ConnectionProperties,
};

#[tokio::main]
async fn main() {
    let url = "amqps://eaxyapkn:tRYdniZqsBBge5J3DXPFdFCqLQGew7FA@gerbil.rmq.cloudamqp.com/eaxyapkn";
    let options = ConnectionProperties::default()
        .with_executor(tokio_executor_trait::Tokio::current())
        .with_reactor(tokio_reactor_trait::Tokio);

    let conn = Connection::connect(url, options).await.unwrap();
    let channel = conn.create_channel().await.unwrap();
    channel
        .exchange_declare(
            "foo-exchange",
            lapin::ExchangeKind::Topic,
            ExchangeDeclareOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();
    let queue = channel
        .queue_declare(
            "foo-queue",
            QueueDeclareOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();
    channel
        .queue_bind(
            queue.name().as_str(),
            "foo-exchange",
            "foo",
            QueueBindOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();

    let consumer = channel
        .basic_consume(
            queue.name().as_str(),
            "foo-consumer",
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();

    consumer.set_delegate(move |delivery: DeliveryResult| async move {
        let delivery = match delivery {
            Ok(Some(delivery)) => delivery,
            Ok(None) => return,
            Err(err) => {
                println!("Failed to consume queue message {}", err);
                return;
            }
        };

        let message = String::from_utf8_lossy(&delivery.data);
        println!("Received a message: {}", message);

        delivery.ack(BasicAckOptions::default()).await.unwrap();
    });
    conn.run().unwrap();
}
